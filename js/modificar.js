const firebaseConfig = {
  apiKey: "AIzaSyC5CIeThlDAd_hjKFiVNWfANZFG8lotY9A",
  authDomain: "proyectofinaladrian-b27a0.firebaseapp.com",
  projectId: "proyectofinaladrian-b27a0",
  storageBucket: "proyectofinaladrian-b27a0.appspot.com",
  messagingSenderId: "778871624496",
  appId: "1:778871624496:web:59d44f65356be5908ceef5"
};

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

window.addEventListener('DOMContentLoaded', (event) => {
  mostrarProductos();
});

var botonAgregar = document.getElementById('botonAgregar');
var botonBuscar = document.getElementById('botonBuscar');
var botonActualizar = document.getElementById('botonActualizar');
var botonBorrar = document.getElementById('botonBorrar');

const imagenS = document.getElementById('imagenS');
const subir = document.getElementById('subir');
const progresoDiv = document.getElementById('progreso');
const txtUrlInput = document.getElementById('txtUrl');

var codigo = 0;
var nombrePro = "";
var precioPro = 0;
var descripcionPro = "";
var urlImg = "";

subir.addEventListener('click', (event) => {
  event.preventDefault();
  const file = imagenS.files[0];

  if (file) {
    const storageRef = ref(storage, file.name);
    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on('state_changed', (snapshot) => {
      const progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      progresoDiv.textContent = 'Progreso: ' + progreso.toFixed(2) + '%';
    }, (error) => {
      console.error(error);
    }, () => {
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        txtUrlInput.value = downloadURL;
        setTimeout(() => {
          progresoDiv.textContent = '';
        }, 500);
      }).catch((error) => {
        console.error(error);
      });
    });
  }
});

function obtenerValores() {
  codigo = document.getElementById('inputCodigo').value;
  nombrePro = document.getElementById('inputNombre').value;
  precioPro = document.getElementById('inputPrecio').value;
  descripcionPro = document.getElementById('inputDescripcion').value;
  urlImg = document.getElementById('txtUrl').value;
}

function mensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => {
    mensajeElement.style.display = 'none';
  }, 10000);
}

function capturar() {
  obtenerValores();
  if (codigo === "" || nombrePro === "" || precioPro === "" || descripcionPro === "" | urlImg === "") {
    mensaje("Captura la información");
    return;
  }
  set(refS(db, 'servicios/' + codigo), {
    nombre: nombrePro,
    precio: precioPro,
    descripcion: descripcionPro,
    UrlImg: urlImg
  }).then(() => {
    mensaje("Se añadio existosamente");
    limpiar();
    mostrarProductos();
  }).catch((error) => {
    mensaje("Error: " + error);
  });
}

function buscar() {
  let codigo = document.getElementById('inputCodigo').value.trim();
  if (codigo === "") {
    mensaje("Codigo no ingresado");
    return;
  }

  const dbref = refS(db);
  get(child(dbref, 'servicios/' + codigo)).then((snapshot) => {
    if (snapshot.exists()) {
      nombrePro = snapshot.val().nombre;
      precioPro = snapshot.val().precio;
      descripcionPro = snapshot.val().descripcion;
      urlImg = snapshot.val().UrlImg;
      mostrarValores();
    } else {
      limpiar();
      mensaje("El producto no existe");
    }
  });
}

function mostrarValores() {
  document.getElementById('inputNombre').value = nombrePro;
  document.getElementById('inputPrecio').value = precioPro;
  document.getElementById('inputDescripcion').value = descripcionPro;
  document.getElementById('txtUrl').value = urlImg;
}

function mostrarProductos() {
  const dbRef = refS(db, 'servicios');
  const productos = document.getElementById('productos');
  const tbody = productos.querySelector('tbody');
  tbody.innerHTML = '';

  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();

      var fila = document.createElement('tr');

      var celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);

      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.nombre; 
      fila.appendChild(celdaNombre);

      var celdaDescripcion = document.createElement('td');
      celdaDescripcion.textContent = data.descripcion;
      fila.appendChild(celdaDescripcion);

      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = "$" + data.precio;
      fila.appendChild(celdaPrecio);

      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.UrlImg;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);

      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}

function actualizar() {
  obtenerValores();
  if (codigo === "" || nombrePro === "" || precioPro === "" || descripcionPro === "" | urlImg === "") {
    mensaje("Caputura la información necesaria");
    return;
  }
  set(refS(db, 'servicios/' + codigo), {
    nombre: nombrePro,
    precio: precioPro,
    descripcion: descripcionPro,
    UrlImg: urlImg
  }).then(() => {
    mensaje("Informacion actualizada");
    limpiar();
    mostrarProductos();
  }).catch((error) => {
    mensaje("Error: " + error);
  });
}

function eliminar() {
  let codigo = document.getElementById('inputCodigo').value.trim();
  if (codigo === "") {
    mensaje("Codigo Invalido");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'servicios/' + codigo)).then((snapshot) => {
    if (snapshot.exists()) {
      remove(refS(db, 'servicios/' + codigo))
        .then(() => {
          mensaje("Producto eliminado exitosamente");
          limpiar();
          mostrarProductos();
        })
        .catch((error) => {
          console.log("Ocurrió un error al eliminar el producto: " + error);
        });
    } else {
      limpiar();
      mensaje("El ID: " + codigo + " no existe.");
    }
  });
}

function limpiar() {
  document.getElementById('inputCodigo').value = '';
  document.getElementById('inputNombre').value = '';
  document.getElementById('inputPrecio').value = '';
  document.getElementById('inputDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

function validacion(event) {
  var charCode = event.which ? event.which : event.keyCode;
  if (charCode < 48 || charCode > 57) {
    event.preventDefault();
  }
}

document.getElementById('inputCodigo').addEventListener('keypress', validacion);
document.getElementById('inputPrecio').addEventListener('keypress', validacion);
botonAgregar.addEventListener('click', capturar);
botonBuscar.addEventListener('click', buscar);
botonActualizar.addEventListener('click', actualizar);
botonBorrar.addEventListener('click', eliminar);
