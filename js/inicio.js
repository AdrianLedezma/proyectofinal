document.addEventListener("DOMContentLoaded", function() {
    const newsImage = document.getElementById("news-image-link");

    newsImage.addEventListener("click", function() {
        window.location.href = "/html/noticias.html";
    });
});

document.addEventListener("DOMContentLoaded", function() {
    const newsImage = document.getElementById("product-image-link");

    newsImage.addEventListener("click", function() {
        window.location.href = "/html/productos.html";
    });
});


document.addEventListener("DOMContentLoaded", function() {
    const images = document.querySelectorAll(".img2");
    let currentImageIndex = 0;

    // Función para mostrar la siguiente imagen
    function showNextImage() {
        images[currentImageIndex].style.opacity = 0;
        currentImageIndex = (currentImageIndex + 1) % images.length;
        images[currentImageIndex].style.opacity = 1;
    }

    // Muestra la primera imagen
    images[currentImageIndex].style.opacity = 1;

    // Cambia la imagen cada 3 segundos
    setInterval(showNextImage, 4000);
});