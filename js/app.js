const firebaseConfig = {
  apiKey: "AIzaSyC5CIeThlDAd_hjKFiVNWfANZFG8lotY9A",
  authDomain: "proyectofinaladrian-b27a0.firebaseapp.com",
  projectId: "proyectofinaladrian-b27a0",
  storageBucket: "proyectofinaladrian-b27a0.appspot.com",
  messagingSenderId: "778871624496",
  appId: "1:778871624496:web:59d44f65356be5908ceef5"
};

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'

const app = initializeApp(firebaseConfig);
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);

const inicio = document.getElementById("inicio");
const emailInput = document.getElementById("Correo");
const passwordInput = document.getElementById("contraseña");
const signInButton = document.getElementById("btnEnviar");
const errorMensaje = document.getElementById("errorMensaje");

inicio.addEventListener("submit", (event) => {
  event.preventDefault();

  const Correo = emailInput.value;
  const contraseña = passwordInput.value;

  signInWithEmailAndPassword(auth, Correo, contraseña)
    .then((userCredential) => {
      window.location.href = "/html/modificar.html";
    })
    .catch((error) => {
      console.log("Error al iniciar sesión:", error);
      errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});
