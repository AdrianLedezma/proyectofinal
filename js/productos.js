const firebaseConfig = {
  apiKey: "AIzaSyC5CIeThlDAd_hjKFiVNWfANZFG8lotY9A",
  authDomain: "proyectofinaladrian-b27a0.firebaseapp.com",
  projectId: "proyectofinaladrian-b27a0",
  storageBucket: "proyectofinaladrian-b27a0.appspot.com",
  messagingSenderId: "778871624496",
  appId: "1:778871624496:web:59d44f65356be5908ceef5"
};

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, onValue, ref } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

window.addEventListener('DOMContentLoaded', (event) => {
  productos();
});

function productos() {
  const dbRef = ref(db, 'servicios');
  const section = document.querySelector('.servicios');

  onValue(dbRef, (snapshot) => {
    section.innerHTML = '';

    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();

      const productoDiv = document.createElement('div');
      productoDiv.className = 'producto';

      productoDiv.addEventListener('mouseenter', () => {
        productoDiv.classList.add('enfocar');
      });

      productoDiv.addEventListener('mouseleave', () => {
        productoDiv.classList.remove('enfocar');
      });

      const productImage = document.createElement('img');
      productImage.src = data.UrlImg;
      productImage.alt = '';
      productoDiv.appendChild(productImage);

      const productonombre = document.createElement('h3');
      productonombre.className = 'nombre';
      productonombre.textContent = data.nombre;
      productoDiv.appendChild(productonombre);

      const productodescripcion = document.createElement('p');
      productodescripcion.className = 'descripcion';
      productodescripcion.textContent = data.descripcion;
      productoDiv.appendChild(productodescripcion);

      const productoprecio = document.createElement('p');
      productoprecio.className = 'precio';
      productoprecio.textContent = `$${data.precio}`;
      productoDiv.appendChild(productoprecio);

      section.appendChild(productoDiv);
    });
  }, { onlyOnce: true });
}
